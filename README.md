# Backend

### Database
To run postgres in docker container:
1. Pull docker image `docker pull postgres:14.4-alpine`
2. Run `docker run --name postgres-db -d -p 5432:5432 -e POSTGRES_PASSWORD=postgres postgres`

If using different configuration update config file located at `src/main/resources/application.yml`

### Application
`./gradlew bootRun`