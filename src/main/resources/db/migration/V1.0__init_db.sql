create table users
(
    id       bigserial primary key,
    username varchar(50)  not null unique,
    password varchar(100) not null,
    enabled  boolean      not null default true
);

insert into users (username, password, enabled)
VALUES ('admin', '$2a$10$XUz8INFxEnhg5ZCEU2ZPCOGUs/8k04hwPgAeCEkKemIcLuUippd1a', true),
       ('user1', '$2a$10$47cH0syO0yg1tg.jndjkzu49y73aAW3wbxqfinCWkT5YiJTEr0yui', true),
       ('user2', '$2a$10$91dsMtE5.QAdwYgjWxjTtemLY9hUpaaSCoTDRUBHaRCUJIcycJiPi', false);