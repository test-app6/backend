package com.binarapps.task.controller.mapper;

import com.binarapps.task.controller.dto.UserDto;
import com.binarapps.task.model.User;

import java.util.List;

public class UserDtoMapper {
    private UserDtoMapper() {
    } // NOP

    public static List<UserDto> mapToUserDtoList(List<User> users) {
        return users.stream().map(UserDtoMapper::mapToUserDto).toList();
    }

    private static UserDto mapToUserDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .enabled(user.isEnabled())
                .build();
    }
}
