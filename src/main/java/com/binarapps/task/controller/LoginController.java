package com.binarapps.task.controller;

import com.binarapps.task.controller.dto.UserCredentials;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class LoginController {
    @PostMapping("/login")
    public void login(@RequestBody @Valid UserCredentials userCredentials){}
}
