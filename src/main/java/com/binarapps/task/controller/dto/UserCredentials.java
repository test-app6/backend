package com.binarapps.task.controller.dto;

import org.hibernate.validator.constraints.Length;

public record UserCredentials(
        @Length(min = 4, max = 50) String username,
        @Length(min = 8, max = 32) String password) {
}
