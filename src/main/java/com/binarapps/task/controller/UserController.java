package com.binarapps.task.controller;

import com.binarapps.task.controller.dto.UserCredentials;
import com.binarapps.task.controller.dto.UserDto;
import com.binarapps.task.model.User;
import com.binarapps.task.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

import static com.binarapps.task.controller.mapper.UserDtoMapper.mapToUserDtoList;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/users")
    public List<UserDto> getAllUsers() {
        return mapToUserDtoList(userService.getAllUsers());
    }

    @PostMapping("/users")
    public ResponseEntity<User> addUser(@RequestBody @Valid UserCredentials userCredentials) {
        userService.addUser(new User(userCredentials.username(), userCredentials.password()));
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<User> updateUser(@PathVariable Long id, @RequestParam boolean enabled) {
        userService.updateUser(id, enabled);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }
}
